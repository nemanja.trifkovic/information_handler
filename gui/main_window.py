from PySide2.QtWidgets import QMainWindow, QLabel, QDockWidget
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt
from gui.widgets.central_widget import CentralWidget
from gui.widgets.menu_bar import MenuBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.tool_bar import ToolBar
from gui.widgets.structure_dock_widget import StructureDockWidget


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        # poziv super inicijalizaotra (QMainWindow)
        super().__init__(parent)
        # Osnovna podesavanja glavnog prozora
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("resources/icons/blue-document.png"))
        self.resize(1000, 800)

        # inicijalizacija osnovnih elemenata GUI-ja
        self.tool_bar = ToolBar(parent=self)
        self.menu_bar = MenuBar(self)
        self.status_bar = StatusBar(self)
        self.central_widget = CentralWidget(self)
        self.file_dock_widget = StructureDockWidget("Struktura radnog prostora", self)

        self.database_dock_widget = QDockWidget("Baze podataka",self)

        # uvezivanje elemenata GUI-ja
        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.file_dock_widget, Qt.Vertical)

        self.addDockWidget(Qt.LeftDockWidgetArea,self.database_dock_widget,Qt.Vertical)

        # sacuvavanje prijavljenog korisnika iz dijaloga
        self.status_bar.addWidget(QLabel("Ulogovani korisnik: " + "Operativni korisnik"))

    def get_actions(self):
        return self.tool_bar.actions_dict
    
    def add_actions(self, where="menu", name=None, actions=[]):
        if where == "menu":
            # dodati u meni sa nazivom name (file, edit...)
            self.menu_bar.add_actions(name, actions)
        elif where == "toolbar":
            # dodati akcije u toolbar nakon separatora
            self.tool_bar.addSeparator()
            self.tool_bar.addActions(actions)
