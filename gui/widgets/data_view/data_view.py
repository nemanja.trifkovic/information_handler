from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableView, QAction, QFileDialog, QAbstractItemView
from PySide2.QtGui import QIcon
from gui.widgets.data_view.data_model import DataModel
from gui.widgets.edit_dialog.edit_dialog import EditDialog
import csv


class DataView(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.widget_layout = QVBoxLayout()

        # definisanje akcije za otvaranje datoteke
        self.on_open_file_action = QAction(QIcon("resources/icons/folder-open-document.png"), "Open")
        self.on_open_file_action.triggered.connect(self.on_open_file)

        self.table_view = QTableView()
        self.table_view.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table_view.setSelectionBehavior(QAbstractItemView.SelectRows)

        # uvezivanje da kada se dvoklikne na red se otvori dijalog za izmenu podataka
        self.table_view.doubleClicked.connect(self.open_edit_form)

        # Model ce se populisati kada se otvori datoteka
        # self.table_model = DataModel()
        # self.table_view.setModel(self.table_model)

        self.widget_layout.addWidget(self.table_view)
        self.setLayout(self.widget_layout)

    def export_actions(self):
        return [self.on_open_file_action]

    def on_open_file(self):
        # izbor datoteke iz fajl sistema
        file_name = QFileDialog.getOpenFileName(self, "Open data file", "/resources/data", "CSV files (*.csv)")
        # kada se otvori diajlog a ne izabere datoteka
        if file_name[0] == "":
            return
        # ucitavanje podataka iz datoteke
        with open(file_name[0], "r", encoding="utf-8") as fp:
            reader = csv.reader(fp, delimiter=";")
            reader = list(reader)
            header = reader[0] # FIXME: ovo ce biti u meta-podacima
            data = reader[1:]
            # kreiranje modela na osnovu ucitanih podataka
            self.table_model = DataModel(data=data, header_data=header)
            # postavljanje modela u prikaz
            self.table_view.setModel(self.table_model)

    def open_edit_form(self, index=None):
        model = self.table_view.model()
        selected_labels = model.get_headers()
        selected_data = model.get_row(index.row())
        # kreiranje dijaloga
        edit_dialog = EditDialog(self.parent())
        # populisanje dialoga podacima iz reda tabele
        edit_dialog.enter_data(selected_labels, selected_data)
        # prikazivanje dijaloga
        result = edit_dialog.exec_()
        if result == 1: # izmena prihvacena
            data = edit_dialog.get_data()
            model.replace_data(index.row(), data)

