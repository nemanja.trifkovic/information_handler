from PySide2.QtWidgets import QWidget, QVBoxLayout
# from gui.widgets.table_widget.test_table_widget import TestTableWidget
from gui.widgets.data_view.data_view import DataView


class CentralWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.cw_layout = QVBoxLayout(self)
        self.table = DataView()

        # main_window
        self.parent().add_actions(actions=self.table.export_actions(), where="toolbar")
        self.parent().add_actions(actions=self.table.export_actions(), where="menu", name="file")

        self.cw_layout.addWidget(self.table)

        # obavezno dati layout uvezujemo na widget (CentralWidget)
        self.setLayout(self.cw_layout)